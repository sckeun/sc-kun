from flask import Flask, render_template, request, jsonify
from whitenoise import WhiteNoise
import numpy as np
import pandas as pd
import pickle

app = Flask(__name__)
app.wsgi_app = WhiteNoise(app.wsgi_app, root='static/')

@app.route("/")
def index():
    return render_template("home.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/prediction")
def prediction():
    return render_template("form.html")

def predict_value(datum):
    app.logger.debug(datum)
    df = pd.DataFrame(datum)
    model = pickle.load(open("machine_learning/model/entropy.pkl","rb"))
    pe = pickle.load(open("machine_learning/model/predict_enc.pkl","rb"))
    oe = pickle.load(open("machine_learning/model/output_enc.pkl","rb"))

    prediction = model.predict(pe.transform(df))
    entropy = oe.inverse_transform(prediction.reshape(-1, 1))[0][0]

    model = pickle.load(open("machine_learning/model/gini.pkl","rb"))
    prediction = model.predict(pe.transform(df))
    gini = oe.inverse_transform(prediction.reshape(-1, 1))[0][0]

    model = pickle.load(open("machine_learning/model/bayes.pkl","rb"))
    app.logger.debug(model)
    bayes = model.predict(df.to_dict(orient='row'))[0]
    return bayes, gini, entropy

@app.route('/result',methods = ['POST'])
def result():
    if request.method == 'POST':
        data = request.form.to_dict()
        data['jumlahSks'] = int(data['jumlahSks'])
        data['jumlahSmt'] = int(data['jumlahSmt'])

        for k, v in data.items():
            data[k] = [v]

        bayes, gini, entropy = predict_value(data)

        app.logger.debug(result)
        return render_template("result.html",
                prediction_bayes=bayes,
                prediction_gini=gini,
                prediction_entropy=entropy)
