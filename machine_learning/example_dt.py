#!/usr/bin/env python3
import numpy as np
import decision_tree as dt

data = np.array([
    [0,1,2],
    [1,2,3],
    [2,3,4]
])

target = np.array([
    [3],
    [4],
    [5]
])

dtc = dt.Classifier(criterion='entropy')
dtc.fit(data, target)

x = dtc.predict([
    [0,1,2],
    [1,2,3],
    [2,3,4],
    [0,0,0],
])

print(repr(x))
