'''
decision_tree module provides Decision Tree Classifier.
'''
import numpy as np
from random import choice


class Classifier:
    def __init__(self, criterion='gini'):
        '''
        Decision Tree 
        
        Parameters
        ----------
        criterion : string, optional (default="gini")
            The function to measure the quality of a split. Supported criteria are
            "gini" for the Gini impurity and "entropy" for the information gain.
        '''
        self.__criterion = criterion

    def decision_tree_learning(self, examples, attributes, parent_examples=()):
        if not len(examples):
            return plurality_value(parent_examples)
        elif all_has_same_class(examples):
            cls = examples[0,-1]
            return Leaf(cls)
        elif not len(attributes):
            return plurality_value(examples)
        else:
            A = max(attributes, key=lambda a: self.importance(a, examples))
            tree = Tree(A, default_child=plurality_value(examples))
            for vk, exps in split_by(A, examples):
                subtree = self.decision_tree_learning(exps,
                              removeall(attributes, A), examples)
                tree.add(vk, subtree)

            return tree

    def fit(self, data, target):
        examples = np.append(data, target, 1)
        self.tree = self.decision_tree_learning(examples, list(range(data.shape[1])))

    def predict(self, data):
        return np.array([self.tree.predict(datum) for datum in data])

    def importance(self, attribute, examples):
        N = len(examples)
        I = entropy if self.__criterion == 'entropy' else gini

        remainder = sum((len(examples_i)/N) * I(examples_i[:,attribute])
                        for (v, examples_i) in split_by(attribute, examples))

        return I(examples) - remainder


def plurality_value(examples):
    values = examples[:,-1]
    return Leaf(values)

def all_has_same_class(examples):
    cls = examples[0][-1]
    return all(example[-1] == cls for example in examples[1:])

def entropy(target_column):
    # Calculate the entropy of the target column
    elements, counts = np.unique(target_column, return_counts=True)
    entropy = np.sum(
        [(-counts[i] / np.sum(counts)) * np.log2(counts[i] / np.sum(counts)) for i in range(len(elements))])
    return entropy

def gini(values):
    sorted_v = np.sort(values)
    n = len(values)
    cumsum = np.cumsum(sorted_v)
    return (n + 1 - 2 * np.sum(cumsum) / cumsum[-1]) / n

def split_by(attribute, examples):
    return ((v, np.array([e for e in examples if e[attribute] == v])) for v in set(examples[:,attribute]))

def removeall(values, removed):
    return np.array([i for i in values if i != removed])


class Tree():
    def __init__(self, attribute, default_child=None, branches=None):
        self.attribute = attribute
        self.default_child = default_child
        self.branches = branches or {}

    def add(self, value, branch):
        self.branches[value] = branch

    def predict(self, example):
        value = example[self.attribute]
        if value in self.branches:
            return self.branches[value].predict(example)
        else:
            return self.default_child.predict(example)

    def get_depth(self):
        if len(self.branches):
            return max(map(lambda b: self.branches[b].get_depth(), self.branches))
        else:
            return self.default_child.get_depth()


class Leaf():
    def __init__(self, examples):
        if hasattr(examples, '__iter__'):
            self.cls = examples
        else:
            self.cls = (examples,)

    def predict(self, example):
        return choice(self.cls)

    def get_depth(self):
        return 1
