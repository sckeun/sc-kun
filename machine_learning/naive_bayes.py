import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split


def main():
    predict()


class Classifier:
    def fit(self, data, target):
        data['SC'] = target

        self.priors = calculate_priors(data.SC)
        self.means_var = cal_means_var(data)
        self.appearance = cal_appearance_x_given_y(data)

    def predict(self, data):
        results = []
        for datum in data:
            results.append(calculate_bayes(self.priors, self.means_var,
                self.appearance, datum))
        return np.array(results)


def traintest_split(dataset):
    x_train, x_test= train_test_split(dataset,test_size=0.2)
    return x_train, x_test

def calculate_priors(data_target):
    elements, counts = np.unique(data_target, return_counts=True)
    total_data = len(data_target)
    SC_count = {}
    SC_prior = {}
    for i in range (len(elements)):
        SC_count[elements[i]] = counts[i]
        SC_prior[elements[i]] = counts[i]/total_data
    return SC_count, SC_prior

    
# Counting the number of every label in 'SC' 
def cal_means_var(train):
    cont_data = np.array(train.drop(['MD1', 'SDA', 'StatProb'], axis = 1))
    means = {}
    varian = {}
    for i in np.unique(cont_data[:,2]):
        tmp = cont_data[np.where(cont_data[:,2] == i)]
        means[i] = {'jumlahSks' : np.mean(tmp[:,0]), 'jumlahSmt' : np.mean(tmp[:,1])}
        varian[i] = {'jumlahSks' : np.var(tmp[:,0]), 'jumlahSmt' : np.var(tmp[:,1])}
    return means, varian

# Counting the number of appearance of x given y 
def cal_appearance_x_given_y(train):
    data_train = train.drop(['jumlahSks', 'jumlahSmt'], axis = 1).to_dict(orient="row")
    categSC = {'A' : {'MD1' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0},
                      'SDA' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0},
                      'StatProb' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0}},
               'A-' : {'MD1' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0},
                       'SDA' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0},
                       'StatProb' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0}},
               'B+' : {'MD1' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0},
                       'SDA' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0},
                       'StatProb' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0}},
               'B' : {'MD1' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0},
                      'SDA' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0},
                      'StatProb' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0}},
               'B-' : {'MD1' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0},
                       'SDA' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0},
                       'StatProb' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0}},
               'C+' : {'MD1' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0},
                       'SDA' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0},
                       'StatProb' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0}},
               'C' : {'MD1' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0},
                      'SDA' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0},
                      'StatProb' : {'A':0, 'A-':0, 'B+':0, 'B':0, 'B-':0, 'C+':0, 'C':0}}}

    for i in range (len(data_train)):
        categSC[data_train[i]['SC']]['MD1'][data_train[i]['MD1']] +=1;
        categSC[data_train[i]['SC']]['SDA'][data_train[i]['SDA']] +=1;
        categSC[data_train[i]['SC']]['StatProb'][data_train[i]['StatProb']] +=1;
            
    return categSC

# Create a function that calculates p(x | y) for continue values:
# e.g: p (jumlahSks | nilaiSC_A)
def p_x_given_y_cont(x, mean_y, variance_y):
    
    try:
        # Input the arguments into a probability density function
        p = 1/(np.sqrt(2*np.pi*variance_y)) * np.exp((-(x-mean_y)**2)/(2*variance_y))
    
        return p

    except ZeroDivisionError as err:
        if variance_y == 0:
            return 1
        else:
            raise err

# Create a function that calculates p(x | y) for discrete values:
# e.g: p (nilaiMD1_A | nilaiSC_A)
def p_x_given_y_cat(x, matkul, y, categLikeL, total_y):
    
    p = categLikeL[y][matkul][x]/total_y
    return p

def calculate_bayes(priors, means_var, appearance, predictee):
    prob = -1
    for key, value in priors[1].items():
       temp = value * p_x_given_y_cont(predictee['jumlahSks'], means_var[0][key]['jumlahSks'], means_var[1][key]['jumlahSks']) * \
                       p_x_given_y_cont(predictee['jumlahSmt'], means_var[0][key]['jumlahSmt'], means_var[1][key]['jumlahSmt']) * \
                       p_x_given_y_cat(predictee['MD1'], 'MD1', key, appearance, priors[0][key]) * \
                       p_x_given_y_cat(predictee['SDA'], 'SDA', key, appearance, priors[0][key]) * \
                       p_x_given_y_cat(predictee['StatProb'], 'StatProb', key, appearance, priors[0][key])
       if temp > prob:
            prob = temp
            results = key
    return results

def test(data):
    test_data = data[1].to_dict(orient="row")

    priors = calculate_priors(data[0].SC)
    means_var = cal_means_var(data[0])
    appearance = cal_appearance_x_given_y(data[0])

    results = []
    for i in range (len(test_data)):
        results.append(calculate_bayes(priors, means_var, appearance, test_data[i]))

    print('The prediction accuracy is: ', (accuracy(test_data, results)), '%')


def train (data):
    priors = calculate_priors(data[0].SC)
    means_var = cal_means_var(data[0])
    appearance = cal_appearance_x_given_y(data[0])
    
def accuracy(data, results):
    counter = 0
    for i in range(len(results)):
        if data[i]['SC'] == results[i]:
            counter+=1
    return counter / len(results) * 100
            
def predict():
    dataset = pd.read_csv("data/dataset.csv")
    data_splitted = traintest_split(dataset)
    train(data_splitted)
    

if __name__ == '__main__':
    main()
