#!/usr/bin/env python3
from random import choice, randint

grades = 'A', 'A-', 'B+', 'B', 'B-', 'C+', 'C'
grades_frequency = 'A', 'A-', 'B+', 'B+', 'B', 'B', 'B-', 'C+', 'C'

def grade_to_int(grade):
    return 10 - grades.index(grade)

dummy = open('dummy.csv', 'w')

print('MD1,SDA,StatProb,jumlahSks,jumlahSmt,SC', file=dummy)

for npm in range(600):
    md1 = choice(grades)
    sda = choice(grades)
    st = choice(grades)

    score = sum(map(grade_to_int, (md1, sda, st)))

    if score > 25:
        sc = choice(grades_frequency[:2])
    elif score > 18:
        sc = choice(grades_frequency[2:5])
    else:
        sc = choice(grades_frequency[5:])

    if sda == 'A':
        sc = 'A'

    sks = randint(12, 26)
    smt = randint(4, 8)

    print(md1, sda, st, sks, smt, sc, sep=',', file=dummy)

dummy.close()
