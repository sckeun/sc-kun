import numpy as np
import pandas as pd
from pandas.api.types import CategoricalDtype
from sklearn import preprocessing
import naive_bayes as nb
import decision_tree as dt
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
from statistics import mean
import pickle

df = pd.read_csv('.\data\dataset.csv')

grades = ['C', 'C+', 'B-', 'B', 'B+', 'A-', 'A']
sks = list(range(df['jumlahSks'].min(), df['jumlahSks'].max() + 1))
smt = list(range(df['jumlahSmt'].min(), df['jumlahSmt'].max() + 1))

grade_type = CategoricalDtype(categories=grades, ordered=True)

type_map = {
    'MD1': grade_type,
    'SDA': grade_type,
    'StatProb': grade_type,
    'SC': grade_type,
}

df = df.astype(type_map)

# Predict Encoder
pe = preprocessing.OrdinalEncoder(categories=[
    grade_type._categories,
    grade_type._categories,
    grade_type._categories,
    sks,
    smt,
])

# Output Encoder
oe = preprocessing.OrdinalEncoder(categories=[grade_type._categories])

data = pe.fit_transform(df.values[:,:-1])
target = oe.fit_transform(df.values[:,-1:])

clf = dt.Classifier()
clf.fit(data, target)

prediction = clf.predict(pe.transform([
    ['C',  'A',  'C', 20, 5],
    ['A-', 'A-', 'A-', 19, 4],
    ['A-', 'A-', 'A-', 24, 4],
    ['B',  'B+', 'A-', 11, 6],
    ['B',  'A-', 'B+', 22, 5]]))

oe.inverse_transform(prediction.reshape(-1, 1))

kf = KFold(n_splits=5)

accuracies = []

for train_index, test_index in kf.split(data, target):
    train_data = data[train_index]
    test_data = data[test_index]

    train_target = target[train_index]
    test_target = target[test_index]

    clf.fit(train_data, train_target)
    predicted = clf.predict(test_data)
    accuracy = accuracy_score(test_target, predicted)

    accuracies.append(accuracy)

clfe = dt.Classifier(criterion='entropy')

kf = KFold(n_splits=5)

accuracies = []
for train_index, test_index in kf.split(data, target):
    train_data = data[train_index]
    test_data = data[test_index]

    train_target = target[train_index]
    test_target = target[test_index]

    clfe.fit(train_data, train_target)
    predicted = clf.predict(test_data)

    accuracy = accuracy_score(test_target, predicted)

    accuracies.append(accuracy)

clfb = nb.Classifier()

kf = KFold(n_splits=5)

accuracies = []
for train_index, test_index in kf.split(data, target):
    train_data = data[train_index]
    test_data = data[test_index]

    train_target = target[train_index]
    test_target = target[test_index]

    # clfb.fit(train_data, train_target)  #ini apa?
    # predicted = clfb.predict(test_data) #ini apa?

    accuracy = accuracy_score(test_target, predicted)

    accuracies.append(accuracy)

# with open('machine_learning/model/gini.pkl', 'wb') as bayes:
#     pickle.dump(clfb, bayes)
