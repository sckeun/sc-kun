import pandas as pd
import numpy as np
from pprint import pprint

import sklearn.model_selection


def entropy(target_column):
    # Calculate the entropy of the target column
    elements, counts = np.unique(target_column, return_counts=True)
    entropy = np.sum(
        [(-counts[i] / np.sum(counts)) * np.log2(counts[i] / np.sum(counts)) for i in range(len(elements))])
    return entropy


def InfoGain(data, split_attribute_name, target_name="SC"):
    # Calculate the entropy of the target
    total_entropy = entropy(data[target_name])

    # Calculate the values and the corresponding counts for the split attribute
    values, counts = np.unique(data[split_attribute_name], return_counts=True)

    # Calculate the weighted entropy
    weighted_entropy = np.sum(
        [(counts[i] / np.sum(counts)) * entropy(
            data.where(data[split_attribute_name] == values[i]).dropna()[target_name])
         for i in range(len(values))])

    # Calculate the information gain
    information_gain = total_entropy - weighted_entropy

    return information_gain


def ID3(data, originaldata, features, target_attribute_name="SC", parent_node_class=None):
    # If all target_values have the same value, return this value
    if len(np.unique(data[target_attribute_name])) <= 1:
        return np.unique(data[target_attribute_name])[0]

    # If the dataset is empty, return the mode target feature value in the original dataset
    elif len(data) == 0:
        return np.unique(originaldata[target_attribute_name])[
            np.argmax(np.unique(originaldata[target_attribute_name], return_counts=True)[1])]

    # If the feature space is empty, return the mode target feature value of the direct parent node
    elif len(features) == 0:
        return parent_node_class

    # If none of the above holds true, grow the tree
    else:
        # Set the default value for this node --> The mode target feature value of the current node
        parent_node_class = np.unique(data[target_attribute_name])[
            np.argmax(np.unique(data[target_attribute_name], return_counts=True)[1])]

        # Select the feature which best splits the dataset.
        # Return the information gain values for the features in the dataset
        item_values = [InfoGain(data, feature, target_attribute_name) for feature in features]
        best_feature_index = np.argmax(item_values)
        best_feature = features[best_feature_index]

        # Create the tree structure.
        # The root gets the name of the feature (best_feature) with the maximum information gain in the first run
        tree = {best_feature: {}}

        # Remove the feature with the best information gain from the feature space
        features = [i for i in features if i != best_feature]

        # Grow a branch under the root node for each possible value of the root node feature
        for value in np.unique(data[best_feature]):
            value = value
            # Split the dataset along the value of the feature with the largest information gain and create sub_datasets
            sub_data = data.where(data[best_feature] == value).dropna()

            # Call the ID3 algorithm for each of those sub_datasets with the new parameters
            subtree = ID3(sub_data, dataset, features, target_attribute_name, parent_node_class)

            # Add the sub tree, grown from the sub_dataset to the tree under the root node
            tree[best_feature][value] = subtree

        return (tree)


def predict(query, tree, default):
    # Check for every feature in the query instance if this feature is existing in the tree.keys() for the first call,
    # tree.keys() only contains the value for the root node --> if this value is not existing,
    # we can't make a prediction and have to return the default value which is the majority value of the target feature
    for key in list(query.keys()):
        if key in list(tree.keys()):
            # Tell the model what to do when the feature values of these query are not existing in our tree model
            # because non of the training instances has had such a value for this specific feature.
            # So, we return the most frequent target feature value of our training dataset (for convenience)
            try:
                # Address the key in the tree which fits the value for key (key == the features in the query)
                result = tree[key][query[key]]
            except:
                return default
            # We run down the tree along nodes and branches until we get to a leaf node
            if isinstance(result, dict):
                return predict(query, result, default)
            else:
                return result


def train_test_split(dataset):
    # The dataset is divided into a training and a testing set.
    # We drop the index respectively relabel the index
    training_data = dataset.iloc[:80].reset_index(drop=True)
    testing_data = dataset.iloc[80:].reset_index(drop=True)
    return training_data, testing_data


def test(data, tree, default):
    # Create new query instances
    # by simply removing the target feature column from the original dataset and convert it to a dictionary
    queries = data.iloc[:,:-1].to_dict(orient="records")

    # Create an empty DataFrame in whose columns the prediction of the tree are stored
    predicted = pd.DataFrame(columns=["predicted"])

    # Calculate the prediction accuracy
    for i in range(len(data)):
        predicted.loc[i, "predicted"] = predict(queries[i], tree, default)
    print('The prediction accuracy is: ', (np.sum(predicted["predicted"] == data["SC"]) / len(data)) * 100, '%')

def find_default(target_attr):
    elements, counts = np.unique(target_attr, return_counts=True)
    most_freq = np.argmax(counts)
    return elements[most_freq]

dataset = pd.read_csv(".\data\dataset.csv", names=['MD1', 'SDA', 'StatProb', 'jumlahSks', 'jumlahSmt', 'SC'])
default = find_default(dataset['SC'])

training_data = train_test_split(dataset)[0]
testing_data = train_test_split(dataset)[1]

tree = ID3(training_data, training_data, training_data.columns[:-1])
pprint(tree)
test(testing_data, tree, default)