# SC-kun

## Data

We are using dataset.csv which is real data from DSTI to build our model.

## Machine Learning Algorithms

The machine learning algorithms are on naive_bayes.py and decision_tree.py which consists of 
creating decision tree with information gain, as well as decision tree with gini index.
The algorithms we use to build our model can be seen on CreateModel.ipynb.

## Website

Check the web at http://sc-kun.herokuapp.com/